---
title: "Other facts about me"
date: 2023-06-25
---

Here you can find some random and potentially useful facts about me.

## My name

My first name is pronounced `da.ni'ɛ.le`. For native English speakers, pronouncing dan-yell-ae will get you pretty close to it. Feel free to call me Dani (everyone, including my family, calls me that way) or Dan.

My surname on the other hand is a real challenge if you don't speak a southern European language.

## Feedback

I believe that great relationships are based on great feedback. Because of that, I greatly appreciate receiving direct feedback about me, both positive and negative, as soon as possible. While I will accept the feedback, expect me to ask questions about it. The reason I do this is to better explore its details, so I can plan my future actions more carefully. So please don't be afraid and hit me up with your most honest feedback!

Likewise, expect me to provide you feedback whenever I have some. Please let me know how you prefer to receive it, else I will do my best to guess your preference.

## My office hours (in UTC)

I'm a morning person: my attention usually deteriorates as the day goes by. I start working at about 7:40 and leave around 15:20 to go pick up my daughters
from school. After that, at about 15:40, I'm back at my desk to finish my work
day for roughly another hour. I aim at having a 1 hour lunch break from 11:00.

All this is the ideal schedule, which may not always happen in reality. I am very flexible with my schedule and sometimes I even work in the evening. This is particularly useful to keep close contact with folks living in American time zones.

On Monday, Wednesday, and Friday I skip the lunch break to then go training (climbing). This is also blocked in my calendar. This doesn't apply from June to early September as the gym follows summer schedule.

## Other facts about me

- Although I'm not at the clinical level, I think I have some degree of [OCD][ocd]. I'm the kind of person who feels the urge to sort a list alphabetically. Yes, the lists of markdown links at the bottom of each page are sorted.
- I don't have any sort of university degrees. I dropped off from computer science engineering university after a year because I was getting bored. So I trained myself to be a systems engineer instead.
- I sometimes make very technical/nerdy jokes. I try to refrain myself when I realise that the audience can't get them, but sometimes they slip out. Please accept my apologies in advance if that happens to you.
- On a similar note, I'm the one who throws jokes in the middle of an outage. Some people may label this as unprofessional or even feel insulted. The reality is that I have been in far more outages, some of which almost catastrophic[^1], than I can remember. The most important lesson that I've learned is that desperation will only have a negative impact on the situation. Conversely, keeping a good mood in the face of adversity helps pulling the best out of engineers and keep them engaged.

[^1]: Remember the GitLab database outage from January 31th 2017? I was part of the production team when that happened. You can spot me in [the livestream on YouTube][gitlab-db-outage-youtube] as we were recovering the data.

[gitlab-db-outage-youtube]: https://www.youtube.com/watch?v=v0TRHLvYGE0
[ocd]: https://en.wikipedia.org/wiki/Obsessive%E2%80%93compulsive_disorder
