---
title: "Reading"
date: 2022-11-25
---

When I took on my first management role[^1], I immediately realised that I had no idea about what the job was going to be for me. Being an engineer, my first reaction was to RTFM, but management is a deep and varied discipline and there isn't a One Book to rule them all.

I've always been an avid reader, so there was only one thing that I could do: read all I could, in any form or medium. I even started to listen to audiobooks.

What follows is a list of resources that I found particularly interesting and useful in my journey so far, some of which aren't at all about management. Have you got some suggestions for me? I'd love to add them after reading!

---

#### Chris Voss - [Never split the difference][never-split-the-difference]

The best negotiation book out there. Packed with techniques and insights that are useful both at work and in private lives.

#### Dale Carnegie - [How to Win Friends and Influence People][how-to-win-friends]

An all-time classic. The basics of how to establish great relationships with others.

#### Eliyahu M. Goldratt - [Critical Chain][critical-chain]

Project management isn't all that dreadful and scary if you set up the right strategy. This books shows you how.

#### Eliyahu M. Goldratt - [The Goal][the-goal]

The bible of effective management at scale. I discover something new every time I re-read this book.

#### Gene Kim, Kevin Behr, George Spafford - [The Phoenix Project][phoenix-project]

The adaptation of "The Goal" to the modern world of software companies.

#### Ichiro Kishimi, Fumitake Koga - [The Courage To Be Disliked][the-courage-to-be-disliked]

A classic philosophical conversation between a youth and a philosopher about achieving happiness in life by trusting oneself and living in harmony with the rest of society. Do you ever wonder why sometimes people don't understand or accept you? This book is for you.

#### Jason Fried, David Heinemeier Hansson - [It doesn't have to be crazy at work](https://basecamp.com/books/calm)

In the long run, calm is far more productive than working long hours and unrealic deadlines. And it's also more enjoyable for yourself. As counterintuitive as this may sound, it is in fact a concrete choice that many companies simply don't take.

#### Marcus Buckingham, Curt Coffman - [First, Break All the Rules][first-break-all-the-rules]

People always talk about talent, but how many can actually define it? Once you know what it is, you'll look at your teams with a completely different eye.

#### Michael Bungay Stanier - [The Coaching Habit][coaching-habit]

Coaching, the art of guiding someone through the unknown until they reach mastery, goes against the human nature. This book shows techniques to become great at it.

#### Pablo Carranza - [On-Site Friendly][on-site-friendly]

A post about remote work I helped writing a while ago. If remote work scares you, this is a great start.

#### Patrick Lencioni - [The Five Dysfunctions of a Team][dysfunctions]

Team work can be often tricky to achieve. This book shows some common problems and techniques to overcome them.

#### The Arbinger Institute - [Leadership and Self-Deception: Getting out of the Box][getting-out-of-the-box]

An interesting take on itnerpersonal relationships. You may not have realised, but you might be in the box with someone. Try and acknowledge it, then improve your relationship.

---

[^1]: I accepted the challenge on 2018-11-28.

[coaching-habit]: https://www.amazon.com/dp/0978440749
[critical-chain]: https://en.wikipedia.org/wiki/Critical_Chain_(novel)
[dysfunctions]: https://en.wikipedia.org/wiki/The_Five_Dysfunctions_of_a_Team
[first-break-all-the-rules]: https://en.wikipedia.org/wiki/First,_Break_All_the_Rules
[getting-out-of-the-box]: https://www.amazon.com/dp/B07DKHH1GC
[how-to-win-friends]: https://en.wikipedia.org/wiki/How_to_Win_Friends_and_Influence_People
[never-split-the-difference]: https://www.amazon.com/dp/B018FHCPDO
[on-site-friendly]: https://cabify.tech/culture/on-site-friendly/
[phoenix-project]: https://www.amazon.com/dp/B078Y98RG8
[the-courage-to-be-disliked]: https://www.amazon.com/dp/B078MDSV8T
[the-goal]: https://en.wikipedia.org/wiki/The_Goal_(novel)
