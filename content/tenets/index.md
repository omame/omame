---
title: "Tenets"
date: 2023-05-24
---

Here are some of the tenets that guide me.<br />You'll hear me saying these regularly.

## Hope is not a strategy

Literally the first sentence of the [Google SRE book][google-sre-book], this is something to always keep in mind when planning something, be that an important project at work, a trip to a holiday destination, your favourite hobby, and so on. Always ask yourself "how much of this plan is based on logic and facts, and how much relies on hope?". Setting up a strategy with a high degree of hope is essentially the same as gambling: some times it may go well, often it will not, and in the long run the balance will always be negative. If you are a C-level and find yourself using the verb "hope" a lot when talking about your plans, please stop for a moment and reconsider what you are doing with your company's strategy.

## Less is more

Focus is paramount for me. Implementing healthy constraints ([more on this below](#constraints-are-liberating)) reduces entropy and maximises throughput. Humans have limited cognitive capacity, thus we must embrace this natural constraint and exploit it. Clearly, the best way to do this is to do less at the same time, do it better, and consequently maximise throughput. This is true for personal development but it becomes even more important when managing a team.

For example, every team has a certain knowledge base that new joiners are required to acquire as soon as they possibly can. In some kinds of teams, this can appear as a steep mountain to climb to the poor new joiner. I have seen some who were truly scared by the sheer magnitude of this cognitive burden that they were beginning to reconsider their own choices. This usually happens because a small team provides too many services, or the tools they use are too scattered. It's the job of a leader to identify this problem and then implement strategies to reduce the size of the knowledge base, like reorganising the team or rationalising the tools to a smaller subset.

Another example, possibly even more important, is with team workflow. Teams who don't have someone to protect them from external requestors inevitably end up taking far more work than what they are able to process. Context switching, a very expensive state that does more harm than good, is really high and work can't proceed at reasonable speed, sometimes at all. This leads to frustration on both sides and potentially even burnout. This is when a manager's ability to say "no" and negotiation skills need to shine, to defer incoming workload and set realistic delivery expectations to protect the team's focus on a very small number of tasks or projects.

## Constraints are liberating

As counterintuitive as this may sound, often having fewer options translates into more freedom, higher quality, and a better user experience. This is easier to understand within the context of a service provider and its consumer.

One can provide a number of services that satisfy the fragmented demand of consumers. That is, for each consumer requirement there is a service that satisfy that specific demand. With this, the service provider must maintain a number of services that scales linearly to the number of different requirements. In this scenario, maintaining high quality is very expensive and difficult: the scope is too broad and there are too many moving pieces to guarantee a decent service level.

The solution to this is to limit the options and provide a service so good that consumers are willing to accept the tradeoffs and embrace it. The outcome is a far more polished and reliable service at a fraction of the cost. From the consumer perspective, this means spending less time thinking and being frustrated. In a software development scenario, this translates is more time spent coding to generate business value.

This is the more generalised version of the [Unix philosophy][unix-philosophy] of "Do one thing and do it well".

## There is no such thing as finished product

Within the context of a reasonably sized product and much like security, product development is a process, not a finite state. In other words, you can never stop developing a product, be that by adding new features or fixing bugs. This may feel like a sisyphean effort, but accepting this fact greatly helps setting more effective strategies and living free from the utopia of working hard to complete the product: there is no such thing.

## Bias to action

Discussions are important, participation is great, but at some point thing must happen. There is a time and space for philosophy (which I enjoy, in fact) but more often than not results won't come unless actions happen. So after a fruitful and inclusive conversation, someone has to call timeout and make a call. Failing to do so will result in analysis-paralysis and stagnation. A common reason for this is seeking consensus, which must be prevented by [disagreeing and committing][disagree-and-commit].

Of course, bias to action has its tradeoffs. Similarly to [the above point](#there-is-no-such-thing-as-finished-product), you can only go so far into planning actions. You cannot foresee all possible cases and the perfect execution of your plan. Things go sideways all the time and perhaps you didn't consider that tiny detail that seemed insignificant but turned out to be a showstopper. But that's fine as long as you plan for it. Bias to action means you accept this level of uncertainty and move on with actions in a way that can fail fast and be easily rolled back. This fosters experimentation and innovation at a fast pace.

[disagree-and-commit]: https://en.wikipedia.org/wiki/Disagree_and_commit
[google-sre-book]: https://sre.google/sre-book/introduction/
[unix-philosophy]: https://en.wikipedia.org/wiki/Unix_philosophy
