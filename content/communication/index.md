---
title: "How I communicate"
date: 2023-05-24
---

The most important part of building great relationships is establishing a solid communication protocol. In this page you will find some notes about how I tend to communicate, with the goal of explaining the reasoning behind my behaviours. If you notice something missing, and you probably will, I would be grateful if you could report in an issue or -even better- send a merge request.

## Transport

When it comes to work, here are my preferred ways to communicate, sorted descending:

- GitLab/GitHub issues
- [Chat rooms](#chat)
- [Chat private messages](#chat)
- [Video calls](#video-calls)
- Email

As I am fully remote, in-person communication isn't an option for me unless one of us catches a flight.

## Directness

I am very open and I naturally default to not really circling around what I want to say. Some people appreciate this directness, others don't. I strive not to be too direct by default, but if you belong to the latter please let me know and I will adjust myself. I really mean it: if you make me aware then we will level up our communication, with great benefits for all. I can take a slap (actually, many) in the face, so hit me! This has yielded amazing results in the past so please take it as an opportunity.

## Asynchronicity

From the above list, you will notice that I strongly prefer asynchronous over synchronous communication, although I don't despise the latter. Different kinds of messages require different levels of synchronicity. This is why rely on many communication transports instead of just one: use the right tool for the right job.

One of the biggest benefits of asynchronous communication is that is enables me (and you, in fact) to focus on what I'm doing for all the time it requires [rather than being interrupted](https://imgur.com/3uyRWGJ), increasing my efficiency.

## Chat

### Why sometimes it takes me a long time to reply to messages

To me, chat is a semi-synchronous communication tool. Many people have the expectation of synchronicity when writing chat messages, but the reality is that it can (and should) also be used more asynchronously.

In my experience, the best way to use the chat transport is more casual: who happens to be a around participates to the discussion, the rest will catch up whenever they can and contribute asynchronously.

Therefore, please don't expect me to always reply immediately to your messages, especially over chat: I'm not ignoring you, just focusing on something else. I will eventually reply, I promise!

### Why I always appear as away

I am set myself as away on all chat clients that supports presence. This is a way to reinforce the above point. Generally speaking, if someone sees me as online then their expectation is for me to reply immediately. This may not happen and will lead to frustration from the other part, which I would rather avoid. Thus, I never go online on chats.

### Why I prefer writing in chat rooms to private messages

There are a number of benefits in having conversation in the open. It fosters cooperation, it's more inclusive and transparent. The shared knowledge contributes to creating a sense of community in which everyone can contribute.

There are, of course, topics that must be discussed in private, but defaulting to that will only build silos and cause more harm than good.

## Video calls

### Why I sometimes interrupt when you are talking

My brain can't elaborate information batches that are too big. In systems terms, I have a small brain buffer. So expect me to interrupt and ask you to take a couple of steps back if you try and make more than one point during a conversation. If I don't do that, then I will buffer overflow and what I want to say will be evicted from my brain: you will lose a chance to know my opinion.

### Why I often type during a call

I take notes during a call. This both helps me remembering the key points and leaves a written trail that we can review at a later stage: _"[Verba volant, scripta manent][verba-volant]"_. If I have to reply to an urgent chat, I will excuse myself and let you know, most likely I will even tell you what it's about.

[verba-volant]: https://en.wikipedia.org/wiki/Verba_volant,_scripta_manent
